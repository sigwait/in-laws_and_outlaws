<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="style.css" type="text/css" />
  </head>
  <body lang="en">

    <h2>First Entry</h2>

    <p>
      Books on how to succeed, of which this is the latest and best,
      are seldom in disagreement about what success can be taken to
      mean. The expression 'to make good', as colloquially used, may
      not appeal (and in fact doesn't appeal) to the philosopher or
      moralist, but it has at least the merit of being understood. We
      know, roughly, what is meant. We picture the successful man as
      one with high office, secure future, untarnished repute and a
      good press; all gained or improved by his own effort. Our mental
      image shows successively the house by the lake, the mature
      garden, the daughter's jodhpurs and the Georgian silver. With
      change of focus we picture the panelled office, the shining
      desk, the expensive tailoring and the silent car. A last angle
      shot reveals the exclusive club, the central figure of the
      central group, the modest disclaimer of sincere applause. For
      what? Victory, promotion, honours or another son? All four
      perhaps. That is what success commonly means and that is the
      sense in which the term will here be used.
    </p>

    <p>
      It is not to be denied that other forms of success exist, from
      canonization downwards. Some have given their names to exotic
      blooms or else dashed off immortal verse.  Others have lived to
      an improbable age or played in concerts from the age of
      eight. Success can take this form or that. Granted that this is
      so, the reader must nevertheless expect no more than he will
      find. If the success in which he is interested is of the sort
      here first described—the sleek convertible, the private
      beach—this book tells all.  Should he picture success in terms
      of reward in an afterlife or indeed of posthumous fame in this,
      he should seek elsewhere. No tips on martyrdom are offered
      between these covers. Where the word 'success' is used, success
      in the material sense is intended. But while the success under
      discussion is material, it is not crude. For our present
      purpose, the man who makes good has not merely made
      money. Wealth by itself, without prestige and popularity and
      marred, perhaps, by parsimonious piety, is merely a form of
      paralysis. The art of success may involve money but must also
      involve sliding easily into the governing section of society,
      conveying the vague impression of having been there all
      along. In the days when motion pictures used to offer
      entertainment it was the custom of the impoverished to walk into
      a cinema backwards through the exit saying to each other 'That
      was a rotten film.' In much the same way the technique of
      success implies an unobtrusive entry into the ranks of the
      privileged. One must go in with all the boredom of one who is
      coming out. It is not enough to enter: one must also <i>belong</i>.
    </p>

    <p>
      The advice which follows on how to succeed assumes, then, a
      special form of success. It also assumes that the reader is an
      average person but with something less than average ability. We
      too often see books on 'How to Succeed' in which the student is
      urged to be more energetic, more intelligent, more helpful, more
      painstaking, more pleasant and more loyal than anyone else. But
      if he has all those merits, he has no need for the book. It is
      not for that sort of person that the book should be written. He
      will succeed anyway. The person in need of advice is <i>below</i>
      average—stupid, idle, careless, un-co-operative, ill-tempered
      and disloyal. It is for him that books should be written. After
      all, this is a democratic country. Why shouldn't he succeed like
      anyone else? He can, and we shall presently explain how. We
      shall assume for this purpose that success is to be in a field
      of activity to which most people are assigned and for which all
      the others are clearly destined: the field of public and
      business administration. People imagine for themselves a career
      in agriculture, research, stock-breeding, literature or
      field-anthropology. Each sees himself as test-pilot, secret
      agent, ace reporter or cowboy. All will end, if successful, at a
      desk; and it seems to make little difference, in practice,
      whether the desk is at a university, on a rocket range, on a
      farm or in Whitehall. Come what may, that desk awaits each one
      of us. We might just as well realize this from the start,
      beginning as we mean to continue. And once seated at a desk, our
      problem is how to move from desk to desk until we reach the
      topmost desk of all.
    </p>

    <p>
      The reader must be warned, however, against expecting more than
      the author has to offer. This book includes no advice on how to
      succeed in politics. On this subject a useful book might be
      written, but this is not it. A career in politics may have much
      to commend it but its rewards are reserved, in the main, for
      those who have been successful already. On the one side of the
      House, a wealthy family background is not so much required as
      assumed.  On the other, political eminence is hardly to be
      counted as success at all. Towards grasping the reality of
      politics a first and useful step is to study <i>Who's Who</i>,
      an invaluable work of reference which includes a short biography
      of all who could be called eminent in public life.
    </p>

    <p>
      A study and analysis of these biographies will reveal the
      foundation upon which each great career is built. To perform
      this task thoroughly would mean the writing of a book on that
      alone. But even a cursory study of a hundred names may suffice
      to explain the elements of success. Such a study the author of
      this book has made, the statistical material being provided by
      the Conservative government of 1955-56. If Ministers and
      Under-Secretaries are taken, including all who held office even
      for a part of the period; if to these are added the Treasury
      commissioners and law officers; and to these again are added the
      Speaker, the Chairmen of committees, Black Rod and the
      Sergeant-at-Arms, we arrive without much distortion at the
      useful total of one hundred. Of this hundred 81 were educated at
      a public school, 70 at Oxford or Cambridge and 13 at Sandhurst,
      Woolwich or Dartmouth. Of the public-school men no less than 27
      were Etonians, Harrow and Winchester providing only five each,
      Rugby only three and Marlborough and Uppingham only two. Schools
      with a single representative included Westminster, Charterhouse,
      Cheltenham, Radley, Malvern and Stowe. Of the university men, 45
      came from Oxford and only 25 from Cambridge. Of the Oxford men,
      13 were from Christ Church, and of the Cambridge men, 12 were
      from Trinity.  Of the 13 from Christ Church, no less than 10 had
      also been at Eton. From statistics of this kind a certain
      pattern begins to emerge. One may not have been present when the
      government was formed but one has, from internal evidence, a
      clear notion of how it must have been done.
    </p>

    <img src="ch01.0019.svg" alt=""/>

    <p>
      It would not be difficult to reconstruct the sort of marking
      system which must be used in political circles—and this has
      already been done. Marks are awarded for Eton, Christ Church,
      the Guards and for a suitable father-in-law, with a special
      increment for being related to the Duke of Devonshire. But the
      Parkinson Scale has also made it possible to draw up the perfect
      entry in <i>Who's Who</i>; perfect, that is, for one just
      entered on a distinguished political career. It would read
      something like this:
    </p>

    <blockquote>
      <b>UPTON-CUMMING, Neville Edmund Windrush</b>,
      M.B.E. Parliamentary Private Secretary to H.M.'s Secretary of
      State for Foreign Affairs, M.P. (C) for Alltory (West) since
      1953, Director of Upton Plastics, Ltd., Slough; <i>b.</i> 1926,
      2<sup>nd</sup> <i>s.</i> of the late Arthur Upton and Martha,
      <i>o.d.</i> of Hiram B. Cumming of Pittsburg, U.S.A.; <i>m.</i>
      1951 the Hon. Sheila Swellyn, <i>y.d.</i> of the 1<sup>st</sup>
      Lord Innercausus (<i>q.v.</i>) and of Lady Elizabeth, <i>e.d.</i> of
      the 14<sup>th</sup> Earl of Normantowers (<i>q.v.</i>) and widow of the
      Rt. Hon. Sir Cecil Sleightly-Cavendish (<i>d.</i> 1917). One
      <i>s.</i> Sandhurst O.C.T.U. 1940, commissioned in Grenadier
      Guards, served in Italy, mentioned in despatches and awarded
      M.B.E. <i>Educ.</i>: Eton, Christ Church, Oxford, M.A.,
      1<sup>st</sup> Class Hon. Mods., Racquets Blue. President of the
      Oxford Union, 1949. Barrister, Inner Temple, Major in
      Barsetshire Yeomanry (T.A.). Travelled in Outer Mongolia, with
      Public School Expedition to New Guinea, and was correspondent in
      Korea. <i>Publications: In darkest Mongolia, My Plan for
      Employment and The Coming Five Years. Recreations:</i> fishing,
      squash, collecting snuff-boxes. <i>Address:</i> 203 Cadogan
      Mews, Sloane Square, S.W.3, and Oakrafter Lodge, near Newbury,
      Berks. <i>Clubs:</i> Carlton, Pratts, Guards, Cavalry.
    </blockquote>

    <p>
      The discerning reader will observe that the politician to whom
      this entry refers has a 100% rating on the Parkinson Scale—a
      score far higher than any real statesman can actually claim. He
      also has a range of useful adjuncts—a reputation as author and
      traveller, academic, athletic and military achievement, the
      Presidency of the Oxford Union and at least one whimsical hobby
      which seems out of character with the rest. Here is an ideal to
      aim at, preferably from birth; but the astute reader will have
      noted that Mr Upton-Cumming has one qualification which his
      <i>Who's Who</i> entry implies but never states. He is obviously
      <i>rich</i>. It is not for him, therefore, that this book is written.
      He can presumably look after himself. The author seeks
      rather to help those who began with less, those for whom
      politics are out of the question.
    </p>

    <p>
      For success in administration all that we need at the
      outset is the knowledge of how to cheat in Intelligence
      Tests and how to dodge the Personality Screen. All this is
      childishly easy, the tests being designed merely to exclude
      (and very rightly) those to whom even cheating is too
      much bother. We shall assume that these tests have been
      passed and that the reader already has a desk of sorts.
      What do you do next? The chapters which follow provide
      the answer. The assumption which seems to underlie the
      advice given is that the reader is in business and that his
      ultimate ambition is to become Managing Director of a
      mammoth Group of Associated Companies. In many
      instances this may be true, but the counsel offered is just
      as readily applicable to any other form of administration.
      One office is much like another and the principles as here
      laid down will apply to all. Given a desk (the basic need)
      with in-tray, out-tray and pending, telephone, blotter and
      memo pad, the technique of success will always be the
      same. Whatever your actual career may be, whether in
      governmental, municipal, local or business administration,
      the same procedures will apply. With this book at your
      bedside, success is practically unavoidable. For the first
      time in history, success has been made possible for all.
    </p>

  </body>
</html>
