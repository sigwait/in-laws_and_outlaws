<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="style.css" type="text/css" />
  </head>
  <body lang="en">

    <h2>In-laws and Outlaws</h2>
      
    <p>
      To have the right background, at least by repute, and to have in
      addition a name for travel, knowledge and adventure (whether
      earned or skilfully assumed) is to gain, at the outset, a
      considerable advantage. Your more humdrum competitors must seem
      colourless by comparison and barely distinguishable from each
      other. Still more vital, however, is the next step in your
      career. Your main difficulty, as you will by now have
      discovered, is not in forming the right judgement but in
      reaching a first position in which judgement is required. There
      are several ways in which this can be done, one being to model
      yourself on men who have succeeded, another being to take a
      correspondence course in leadership. But the best method of all
      is actually the simplest. It consists in marrying the right
      girl.
    </p>

    <p>
      How is this done? You must study, first of all, the glossy
      magazines, in which photographs of desirable girls alternate
      with photographs of desirable country properties.  The
      day-dreaming and penniless bachelor can pick his country
      estate—these being all for sale—and then decide whether the girl
      on the frontispiece would go well with the architecture and
      scenery as advertised. She sometimes turns out to be already
      engaged (rather oddly) or even married to someone else. In other
      glossy magazines girls as exquisite appear in scenes of an
      elaborate informality.  'The Hunt Ball of the Harkaway, held at
      Canterley Court. Miss Harriet Forrard chatting with the
      Hon. Archibald Frankleigh-Sopping.' The day-dreaming bachelor
      decides that she had better beware—Archibald is obviously not
      good enough. In other photographs Miss Patience Softleigh
      appears simply 'with a friend'. An occasional girl is shown
      leading in the winner at Newmarket or holding the cup she has
      won in the point-to-point. About all these girls there lingers a
      slight, and perhaps sometimes misleading, air of being
      available. A bachelor might be forgiven for thinking they were
      all in the market.
    </p>

    <p>
      It should be made clear at the outset that these glossy
      photographs do <i>not</i> afford a good approach to the subject
      you have now to study. To what extent and in what sense these
      girls are in the market is not to your purpose. The clue to what
      you want is to be found in the same magazine but on a different
      page. The principle upon which you planned your travels was to
      start with the dust-cover of the book which would result. The
      principle upon which you plan your engagement should be exactly
      the same.  You study the photographs of the Society Weddings and
      adapt the biggest of them to your own purpose. You may safely
      assume that prominence will be given to the bridegroom who is
      judged to be a Rising Man. As that is what you intend to be, you
      will plan your affairs accordingly.  Should the photograph
      reproduced on page 17 reproduce a scene outside the Guards
      Chapel, with a double line of brilliantly uniformed officers
      making an arch of swords for the Happy Pair—Captain the Lord
      Utterleigh Broke with his bride the former Miss Penny
      Lespender—you will realize that your wedding will have to be
      different. If you were in that sort of position you wouldn't be
      reading a book like this—nor probably reading a book at all. No,
      the picture to interest you should have a caption worded
      somewhat as follows:
    </p>

    <img src="ch03.0039.svg" alt=""/>

    <blockquote>
      Neville Upton-Cumming with his bride, the Hon. Sheila
      Swellyn, after their marriage on May 14<sup>th</sup> at St Margaret's,
      Westminster. The bride who wore ... was given away by
      her father, Lord Innercausus. The bridesmaids were the
      bride's cousins, Miss Prudence de Benture and Miss Marion
      Fermgilt-Edgerton. The Viscount Hardcurrency acted as
      best man and the guests included the Duke and Duchess of
      Cannonstreet. ...
    </blockquote>

    <p>
      Study this picture carefully and you will conclude, correctly,
      that the bride's father is the central figure, Mr Upton-Cumming
      being essentially his son-in-law. After giving the matter some
      further thought you will realize that Society is largely
      composed of sons-in-law; a fact which has only recently been
      established and one of which only a few are as yet aware. The
      fact for a rising man to grasp is that your father is given you
      and there is little you can do about it; but your father-in-law
      you can choose.  Upon the choice of a father-in-law much of your
      future career may, moreover, depend. Assuming that you have
      successively made yourself a public school man, a traveller, a
      graduate and businessman, you have now a crucial decision to
      make. You must choose your father-in-law, first looking round
      carefully to see what fathers-in-law there may be available. To
      list the sources of information which should be consulted, to
      analyse their authenticity, to arrive at a reasonably accurate
      process of assessment and to define the principles upon which a
      selection should be made—all this would be a Ph.D thesis in
      itself. All that can now be attempted is to dispel some
      illusions and propound a few basic axioms.
    </p>

    <p>
      First, as regards illusions, the suggestion has been made that
      men of ability actually tend to produce more daughters than
      sons. For this theory (except possibly as applied to university
      professors) the evidence would seem to be insufficient. Such
      evidence, moreover, as we possess tends rather towards a
      different conclusion. If it can be assumed, for example, that
      those recently raised to the British Peerage as Barons are men
      of distinction, influence and wealth (an assumption which might
      admittedly be challenged) the statistics of their offspring
      might be thought significant. Of 100 Barons created since 1937
      and still alive, some 23 are childless; and not unreasonably so
      in the case of those who have never married. The remainder can
      boast some 215 children between them, of which total 112 are or
      were male, 103 are female. Allowing for a percentage of war
      casualties, we might conclude that Barons as a race are
      self-perpetuating but not much more than that. The biology of
      Dukes, Marquises, Earls and Viscounts is by no means the same
      and has been dealt with at length in a volume entitled <i>Fauna
      of the British Isles</i>, the work of a well-known zoologist. It
      is for that reason that the sample hundred Barons excludes those
      subsequently raised to higher rank in the Peerage. Numbers in
      the Baronial family appear to range from 1 to 8, the commonest
      total being 2 or 3. What is depressing about the statistics,
      from the bachelor's point of view, is that only 13 Barons have
      daughters only and of these only 5 have restricted their female
      offspring to one. With only these 5 definite sole heiresses and
      8 more with younger sisters to consider, the field is not and
      has never been extensive. And from this discouragingly low total
      we must make deductions in respect of those over fifty years of
      age, those who turn out to be bankrupt and those—probably the
      remainder—who are married. It would be rash to generalize on the
      basis of these figures, but further research would almost
      certainly justify our tentative conclusion that the chances of
      inheriting baronial estates by marriage are relatively low. The
      sons are far too numerous and the only daughters are far too
      few. These are the sad facts and there is no escaping them.
    </p>

    <p>
      It has been emphasized, in the Introduction to this work, that
      some examples of success in life, although inspiring, are
      somewhat unhelpful. When a list of Ministers is seen to include
      such names as these, the Marquis of Salisbury, the Earl of
      Selkirk, the Earl St Aldwyn, the Lord De L'Isle and Dudley, the
      Earl of Home, the Marquis of Reading, the Earl of Munster and
      the Earl de la Warr, the reader may have his moment of
      despair. He may think that the plan for his success should have
      been made at some earlier date, preferably perhaps some four
      centuries before. The advice 'Be born the son of the 5<sup>th</sup>
      Marquis
      of Salisbury' is undeniably sound: but is it useful?  Some
      reader might be in that favoured position but it is not for him
      that this book has been written. Nor are books on how to succeed
      intended for people born as sons of the 13<sup>th</sup> Duke of Hamilton
      and Brandon. These can presumably look after themselves; nor do
      they form, for bookbuying purposes, a sufficiently large
      public. The advice here offered is for men of an entirely
      different type, for men of ambition upon whose choice of a
      father-in-law much else will depend. It has been shown that
      their chance of inheriting vast estates by marrying an heiress
      is fairly remote. What they can do—what the reader of this page
      can do (if unmarried, male and reasonably young)— is to marry
      into a family with influence.
    </p>

    <p>
      The reader may wonder whether this is really feasible.  It
      certainly is feasible and it is constantly being done. Turn the
      pages of the illustrated magazines until you find the group
      picture of the House Party at Macsporran Castle.  Ignore all the
      chief figures in the scene. Our concern is not with the Earl and
      Countess of Macsporran nor with the Mackintosh of Mackintosh nor
      with Lord Pipeslament, nor even with the Honourable Jean
      Tartan. Look closely, rather, at the two young men who complete
      the group; Mr Nigel Smyth and Mr Christopher Browne. It is not
      obvious, to begin with, why they are there at all. Why ask Smyth
      and Browne rather than Robinson, Baker and Jones? What have they
      which others lack? The matter is not without its element of
      mystery, but the fact of their ubiquity is undeniable. By some
      obscure process, the presence of Smyth and Browne has become
      inevitable.  There they are, each with his tweeds and his
      12-bore, and neither looking particularly abashed in the
      presence of the nobility. They look more as if the grouse-moor
      belonged to them, which it certainly does not. These Smyths and
      Brownes are the future sons-in-law, the Men of Promise.  The
      question is how you are to gain a place among them?  One useful
      step is to learn how to ride and shoot. We may be sure that the
      Mr Smyth who emptied both barrels of his gun into the kilt of
      Lord Pipeslament would never be invited to the castle again. It
      must, however, be admitted that this necessary training is not
      in itself the entire secret of success. If every marksman were
      invited to Macsporran in August, the moor would be unduly
      crowded; a fact which the Earl and Countess will have
      realized. Nonchalant handling of firearms may be essential but
      it is certainly not enough.
    </p>

    <p>
      In approaching this difficult problem the first axiom to
      observe is that you need not try to impress the entire
      nobility. There are young men who appear, no doubt, on
      every possible occasion and in every conceivable guise but
      we can safely assume that you are not to be among them.
      Your better plan will be to choose your father-in-law and
      then begin your campaign from there. Your problem is not
      to impress the whole peerage but gain the good opinion
      of one important man. Who is it to be? The choice
      would be simpler if the possible names had been collected
      in a single work of reference, but nothing of the sort has
      ever been done. You will have realized without prompting
      that your list of possible fathers-in-law must be confined
      to men <i>who have daughters</i>. Many a young man has sought to
      ingratiate himself with a wealthy brewer or newspaper
      proprietor only to find him to be childless or the father,
      merely, of innumerable sons. All this results from sheer
      carelessness and deserves no sympathy. The man whose
      respect you must gain must be influential, wealthy and the
      father of at least one daughter. One with several daughters
      is, of course, preferable, your chances being thereby multiplied.
    </p>

    <p>
      Let us suppose that you have drawn up a short list of six. They
      are, shall we say, a Scottish Peer who has recently become
      Postmaster-General, a Baronet of ancient family with a seat in
      Parliament and an extremely wealthy wife, a Football Pool
      Magnate, the Sultan of Gushing-Arabia, a Greek shipowner and the
      proprietor of the
      <i>Sunday's Wonder</i>. Let us suppose that the
      Postmaster-General is, for some reason or another, the man you
      think most suitable. Let us suppose, further, that he is known
      to have three unmarried daughters; Angela, Barbara and Caroline,
      born in alphabetical order and aged respectively twenty-six,
      twenty-three and nineteen. Their father, the 4<sup>th</sup> Earl of
      Bonniebanks, owns the Clanwhiskey estates in Ellesdeeside and
      has extensive interests in the Argentine, Canada, Panama and
      Nicaragua. The family seems suitable in every way. Which
      daughter is it to be?  The deciding factor is age. A wife should
      be half her husband's age plus seven. If you are aged
      twenty-four, you should try to secure Caroline; if thirty-one,
      Barbara; and if thirty-six, Angela. Should you have planned your
      career with care, Caroline might be first choice, with Barbara
      as reserve. This policy decided upon, it only remains to make
      yourself known to the Earl and Countess; but that, for the
      unenterprising, is the main obstacle. For the reader of this
      book, however, it need not be insurmountable. The paragraphs
      which follow should be studied, therefore, with more than
      usually close attention. One of the betterknown experts on
      modern warfare writes frequently about the Strategy of Indirect
      Approach. Whether that is a good idea in warfare may be matter
      for dispute, but the principle is one certainly applicable to
      the problem now under review. To charge up to the Earl and say
      'I want to marry one of your daughters' would be a tactical
      error. The occasion is one for stratagem, subtlety and
      finesse. And the essence of the technique is to obtain the
      fullest information beforehand.
    </p>

    <p>
      Begin your research by listing and making full notes on
      the Earl's and Countess's nearest relatives; their names
      and whereabouts; their special interests; and whether
      they are on speaking terms with the family concerned.
      Let us suppose your card-index entries read like those
      opposite.
    </p>

    <p>
      There would actually be more than eight entries but
      these few are enough to illustrate the principles by which
      you should be guided. First, decide which is the more
      formidable—the Earl or the Countess. Let us suppose
      that it is the Earl you chiefly have to conciliate. The
      possibilities between which you have to choose appear to
      be these:
    </p>

    <table id="earl">
      <thead>
        <!-- soft hyphens to accomodate narrow screens -->
        <tr>
          <th></th>
          <th>Name</th>
          <th>Fam­ily Re­la­tion­ship</th>
          <th>Real Re­la­tion­ship</th>
          <th>Spe­cial In­ter­ests</th>
        </tr>
      </thead>

      <tbody>

        <tr>
          <td>1</td>
          <td>Viscount Clan­whiskey</td>
          <td>Earl's first brother</td>
          <td>Mutual loathing</td>
          <td>Breeding angora rabbits</td>
        </tr>
        
        <tr>
          <td>2</td>
          <td>Lord Elles­dee­side</td>
          <td>Earl's second brother</td>
          <td>Neutral</td>
          <td>Master of the Deeside Otter Hounds</td>
        </tr>
        
        <tr>
          <td>3</td>
          <td>The Hon. Philip de Canter</td>
          <td>Earl's third brother</td>
          <td>Intimate with Earl</td>
          <td>Collects old pistols</td>
        </tr>
        
        <tr>
          <td>4</td>
          <td>Lady Crystal de Canter</td>
          <td>Earl's only sister, unmar­ried</td>
          <td>Neutral</td>
          <td>Vice-President of the Society for the Prevention of Practically Everything</td>
        </tr>
        
        <tr>
          <td>5</td>
          <td>Mrs Heather Tartan</td>
          <td>Eldest sister of Count­ess married to No. 6</td>
          <td>In­ti­mate with Count­ess</td>
          <td>En­thu­si­ast for Stuart suc­ces­sion</td>
        </tr>
        
        <tr>
          <td>6</td>
          <td>General Douglas Blood­worthy Tartan</td>
          <td>Brother­in­law of Count­ess, married to No. 5</td>
          <td>Friendly with Earl</td>
          <td>Deer­stalk­ing, salmon fish­ing, de­scrib­ing how he won the First World War</td>
        </tr>
        
        <tr>
          <td>7</td>
          <td>Miss Ailsa McGaelic</td>
          <td>Un­mar­ried sis­ter of Count­ess</td>
          <td>Hostile to everyone. Detested by Earl</td>
          <td>Practices Yogi. Spends much of her time in Tibet</td>
        </tr>
        
        <tr>
          <td>8</td>
          <td>Miss Blood­worthy Tartan</td>
          <td>Sister of No. 6</td>
          <td>Friendly with Count­ess</td>
          <td>Head­mistress of St Agath­a's Col­lege for Young Ladies</td>
        </tr>
        
      </tbody>
    </table>

    <ol>
      <li>
        You can write an article to prove that the breeding
        of angora rabbits is a vice beside which the crimes of
        Nero fade into insignificance.
      </li>

      <li>
        You can find an unusual pair of duelling pistols and ask No. 3
        to identify them.
      </li>

      <li>
        You can join the Jacobite Society and ingratiate yourself with
        No. 5.
      </li>

      <li>
        You can write to No. 6 and ask his opinion as to how
        the First World War was won.
      </li>

      <li>
        You can allege that No. 7 has never been to Tibet, or
      </li>

      <li>
        You can lecture to the College for Young Ladies of
        which No. 8 is the Headmistress.
      </li>
    </ol>

    <p>
      There is nothing to prevent you trying all these approaches
      simultaneously. The artistic touch would be to lecture to the
      Young Ladies' College on Old Firearms, ensuring that Nos. 2, 5,
      6 and 7 are present. Use the rare duelling pistols to illustrate
      the lecture, with an angora rabbit as the target. Kill the
      rabbit with the first shot and wound Aunt Ailsa accidentally
      with the second. End the lecture by pointing out that the Stuart
      kings never bred angora rabbits and that the First World War was
      nearly lost by the kind of people who do, but won at the
      eleventh hour by people with experience of deer-stalking. If you
      are not invited to Clanwhiskey Castle after that, it will be
      clear to you that the family is not worth bothering about.  The
      chances are, however, that you will find yourself an honoured
      guest within a matter of weeks, and Caroline will probably be
      told to marry you whether she wants to or not. Should there be
      any sales-resistance on her part, you can probably secure Angela
      by pretending to be interested in Barbara. The engagement
      announced, you will shortly enter the select ranks of the
      world's sons-in-law. Two moments of triumph will mark the
      process even before the wedding takes place. First of these will
      be marked by the appearance of the group photograph:
    </p>

    <blockquote>
      <i>On the moors at Ellesdeeside.</i> The Earl and Countess of
      Bonniebanks, Lady Crystal de Canter, Mrs Heather Tartan, the
      Hon. Philip de Canter and General D. B. Tartan,
      D.S.O. (<i>Seated</i>) Lady Angela and Lady Barbara de Canter,
      with Mr Christopher Browne and Mr Aubrey Reeder.
    </blockquote>

    <p>
      Second will be the appearance of a portrait on the first
      page of a later number:
    </p>

    <blockquote>
      Lady Angela de Canter whose engagement has recently
      been announced to Mr Aubrey Reeder, the prospective
      candidate for Alltory (West).
    </blockquote>

    <p>
      The photograph will be extremely flattering, having been taken
      some six years before. Elsewhere in the same issue will be a
      charming picture of Swindlesham Manor, Sussex, the home of Mr
      Aubrey Reeder. Does such a place exist?  Almost certainly, one
      would imagine, but there is no need for you to own it. A Manor
      is easily borrowed and a picture of one can be borrowed more
      easily still.
    </p>

    <p>
      As from the date of your marriage at a fashionable church you
      will be definitely <i>in</i>. Why? Because your wife's relatives
      cannot afford to have failures in the family.  Their affection
      for you may be strictly limited. Their affection for her may
      tend to diminish. But their own credit is involved, to some
      extent, in your success. For just as you will refer to them,
      casually, as relatives, so they cannot avoid referring (less
      frequently) to you. 'The Supernational Banking Corporation?' you
      will say, 'Of course I know it. My wife's uncle is the
      Chairman.' 'Dorset? No, I have never been there. My uncle by
      marriage keeps a pack of hounds there, however, and I'm looking
      forward to our first visit.' 'Salmon fishing? I never had the
      time until last year, when General Tartan invited me to his
      place in Scotland.' To be able to make these references may gain
      you nothing more than prestige. But what when business
      acquaintances ask Lord Bonniebanks about Angela? 'Shall we be
      seeing your married daughter at Ascot?' and 'How's your
      son-in-law doing?' and 'I hear, Lord Bonniebanks, that you have
      a grandson. Congratulations!' For the sake of his own status,
      your father-in-law cannot afford to have you anything less than
      a Director.  With whatever misgivings, he will have to see that
      you are promoted.
    </p>

    <p>
      You are now one of the world's In-Laws. But what would your fate
      have been had it happened otherwise?  You would have been a
      No-Law or an Outlaw, and these are terms which we must now
      define. A No-Law is an eligible bachelor, one who must make his
      way unaided but about whom there lingers the romance of one
      still available. There are minor advantages in this role,
      provided you are not too low in the firm's hierarchy. The
      typists will work for you more willingly and the mothers of the
      less attractive girls will ask you to parties. Rumours may
      circulate about your broken heart, about the heiress to whom you
      were engaged but who died of diphtheria, about the starlet who
      finally married someone else, about the French Countess whose
      family forbade the match on religious grounds, about the
      Austrian Princess who entered a convent so as to avoid being
      married to your rival. Such rumours will go around, provided you
      originate them, and will do you no harm. Neither, however, can
      they do you much good. As compared with an In-Law the No-Law is
      in a weak position, any hint of mystery about his past being as
      likely to repel as attract. Minor advantages apart, his main
      asset lies in the fact that he may still become an In-Law. As
      compared with the Outlaw, the No-Law is at least a potential Man
      of Promise. His cards are still to play.
    </p>

    <p>
      Hard is the lot, by contrast, of the Outlaw. His mistake has
      been to marry the wrong girl. This comes about, normally,
      through marrying at too early an age. He never forgot that
      blonde at the Comprehensive School. He remained loyal to the
      brunette he loved at College. He fell for the redhead who typed
      his first letters as a young executive. Whichever it is, he has
      tied himself by marriage to the environment from which he is
      trying to escape.  The Mudborough School blonde is attractive
      only by the standards of Mudborough, which are of course
      appallingly low. The College brunette may have been the best of
      the bunch at Liverpool—but what a dreary bunch that must have
      been! As for the office redhead she is merely the smartest girl
      of the five who are still unmarried. It is the itch to marry the
      girl next door which is the mark of those predestined for merely
      average success. For while such a wife may be anxious, in
      general, for you to succeed, she will not want your success to
      go beyond a certain point.  She will retain her Mudborough
      outlook. In a larger circle of more prosperous friends she would
      find herself below the average in looks, family, brains and
      knowledge.  Her unspoken preference is for a society in which
      she can be well above the average. Down to that level she will
      try to keep you and the odds are that she will succeed. It is
      thus usually fatal to marry the girl next door. To this rule
      there is one significant exception. A woman sufficiently
      beautiful can make her way at any level of society. Married to a
      girl of stunning appearance a man may have his moments of
      anxiety but he will not, in general, find that she hinders his
      career.
    </p>

    <p>
      Nubility or fitness for marriage can be measured by a formula,
      here to be revealed for the first time. It depends upon careful
      investigation and survey. Where this is impossible, however, or
      where a snap decision is needed, the generally accepted tests
      can be applied in a shortened form. Look at her eyes to see what
      a girl was born with.  Look at her hands to see what she has
      learnt. Look at her mouth to see what she has become. There can
      be no pretence at accuracy in this hurried valuation but it
      follows the same pattern as the more careful assessment. It is
      obviously preferable to apply the test at leisure, aiming at a
      precision which we know to be finally unattainable but using
      nevertheless such science as we can. From the table which
      follows it will be evident that there are eight general aspects
      of quality. They can either be positive, with values graded from
      A to D, or negative from E to H. Using the methods of simple
      arithmetic, a man of any ambition should normally reject any
      idea of marrying a girl whose negative qualities outweigh the
      positive so that E + F + G + H > A + B + C + D.
    </p>

    <p>
      Girls with a zero score are not and should not be in great
      demand. Girls with a positive rating will have a score from 1 to 100,
      obtained by simple addition, the result being the N.R. or Nubility
      Rating. These totals give us, in turn, the Nubility Classification
      (N.C.) as follows:
    </p>

    <table id="nubility">
      <thead>
        <tr><th>Totals</th><th>Class</th></tr>
      </thead>
      <tbody>
        <tr><td>100-85</td><td>I</td></tr>
        <tr><td>84-70</td><td>II</td></tr>
        <tr><td>69-55</td><td>III</td></tr>
        <tr><td>54-40</td><td>IV</td></tr>
        <tr><td>39-25</td><td>V</td></tr>
        <tr><td>24-10</td><td>VI</td></tr>
      </tbody>
    </table>

  <p>
    Girls with a score below 10 are unclassified.
  </p>

  <table class="girls_score">
    <thead>
      <tr>
        <th>Category</th>
        <th>Plus Factors</th>
        <th>Marks</th>
        <th>Totals</th>
      </tr>
    </thead>
    <tbody>
      
      <tr class="row-first">
        <td rowspan="3">A</td>
        <td>Health and Beauty</td>
        <td>20</td>
        <td rowspan="3">40</td>
      </tr>
      <tr>
        <td>Vitality and Energy</td>
        <td>10</td>
      </tr>
      <tr>
        <td>Intelligence</td>
        <td>10</td>
      </tr>

      <tr class="row-first">
        <td rowspan="3">B</td>
        <td>Good family background</td>
        <td>20</td>
        <td rowspan="3">30</td>
      </tr>
      <tr>
        <td>Athletic Skill and Aptitude</td>
        <td>5</td>
      </tr>
      <tr>
        <td>Knowledge</td>
        <td>5</td>
      </tr>

      <tr class="row-first">
        <td rowspan="3">C</td>
        <td>Loyalty</td>
        <td>10</td>
        <td rowspan="3">20</td>
      </tr>
      <tr>
        <td>Good Disposition and Manners</td>
        <td>5</td>
      </tr>
      <tr>
        <td>Social Ease and Popularity</td>
        <td>5</td>
      </tr>
      
      <tr class="row-first">
        <td>D</td>
        <td>Income and Expectations</td>
        <td>10</td>
        <td>10</td>
      </tr>
      
      <tr>
        <td colspan="3"></td>
        <td>100</td>
      </tr>
      
    </tbody>
  </table>


  <table class="girls_score">
    <thead>
      <tr>
        <th>Category</th>
        <th>Minus Factors</th>
        <th>Marks</th>
        <th>Totals</th>
      </tr>
    </thead>
    <tbody>
      
      <tr class="row-first">
        <td rowspan="3">E</td>
        <td>Poor Appearance and Health</td>
        <td>20</td>
        <td rowspan="3">40</td>
      </tr>
      <tr>
        <td>Idleness and Inertia</td>
        <td>10</td>
      </tr>
      <tr>
        <td>Stupidity</td>
        <td>10</td>
      </tr>

      <tr class="row-first">
        <td rowspan="3">F</td>
        <td>Unpleasant relatives and Friends</td>
        <td>15</td>
        <td rowspan="3">30</td>
      </tr>
      <tr>
        <td>Carelessness and Clumsiness</td>
        <td>10</td>
      </tr>
      <tr>
        <td>Ignorance</td>
        <td>5</td>
      </tr>

      <tr class="row-first">
        <td rowspan="3">G</td>
        <td>Infidelity</td>
        <td>10</td>
        <td rowspan="3">20</td>
      </tr>
      <tr>
        <td>Quarrelsomeness and Bad Manners</td>
        <td>5</td>
      </tr>
      <tr>
        <td>Snobbishness and Unpopularity</td>
        <td>5</td>
      </tr>
      
      <tr class="row-first">
        <td>H</td>
        <td>Extravagance and Indebtedness</td>
        <td>10</td>
        <td>10</td>
      </tr>
      
      <tr>
        <td colspan="3"></td>
        <td>100</td>
      </tr>
      
    </tbody>
  </table>
  
  <p>
    The Male Nubility Rating is obtained by using the
    same table, substituting 'good appearance' for 'beauty' in
    Category A and 'Salary, status and prospects' for 'income
    and expectations' in Category D. A man who fails to
    qualify had best forget the whole idea and turn at once to
    the next chapter. One who classifies, however, can fairly
    decide how high he should aim. A Class III man could
    thus reasonably dream of securing a Class II girl, while a
    man in Class I might normally hope to marry a girl in
    Class I.
  </p>

  <p>
    But the man for whom this book is written, the man who seeks to
    rise in the world, is rarely in Class I, having lost marks heavily
    in family background, knowledge, disposition, popularity and
    income. He is probably in Class III at best. But his policy, as we
    have seen, is to marry a girl with a good family background,
    ignoring the fact that her N.C.  may be as low as IV. He thus
    becomes an In-Law. Had he, however, married the girl next door, he
    would probably have found himself with a wife in Class V or
    VI. This would automatically make him an Outlaw, having married
    two or three Classes below expectation without any compensating
    factor from his wife's background. It is not to be denied that
    Outlaws occasionally succeed in life but for this they need quite
    exceptional gifts. An In-Law can succeed, by contrast, with gifts
    below the average and indeed with no obvious merit of any kind. It
    is for this reason that such emphasis is given here on the
    question of marriage. For should you make an early and unsuitable
    marriage or should your plans for becoming an In-Law go astray,
    you will have to rely only on Yourself in what you will find to be
    a highly competitive world.
  </p>

  </body>
</html>
