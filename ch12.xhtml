<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="style.css" type="text/css" />
  </head>
  <body lang="en">

    <h2>The Third Law</h2>

    <p>
      Let us assume in this last chapter that you, the
      reader of this page, have gained one of the higher positions
      in the modern world. With the help of this book (thumbed
      and dog-eared beneath the pillow) you have gone from
      strength to strength. But pause for a minute. Where is it
      all taking you? What is the final goal?
    </p>
    
    <p>
      Of course there are as many goals as there are people,
      but take as one example the Stupendic Gigantsome Metal
      Company, which has a capital of one billion billion and
      an industrial empire deployed from Iceland to Tasmania.
      The Stupendic came into existence (you will recall) as
      the result of a merger. The old Gigantsome Company (an
      amalgamation of Sabre Tooth, Tiger Ltd with Fluttering
      and Swoon Brothers) was taken over by Stupendic, Mammoth
      and Topless Ltd (itself the sequel to the purchase
      of Soaring, Cloudcapped and Dizzy by Bandersnatch,
      Clawtalon and Clutch). So the Stupendic Gigantsome has
      come to bestride the bleak landscape of the modern world.
      Millions stand within its shadow, looking to it for protection
      and livelihood. Whether as executives, technicians,
      shareholders or workers; whether as subsidiary allies,
      sub-contractors, retailers or dependents, they all gaze upwards in
      mute adoration. Nor has their loyalty been thrown away, for
      Stupendic has provided many of them with all they dared
      to ask. This is today's Leviathan, the image before which
      its devotees are prostrate, the deity to which daily sacrifices
      are made, the throne before which petitions are cast.
    </p>
    
    <p>
      But there are signs of revolt. Were you to wander unnoticed
      through London clubs and lobbies you would
      overhear the murmurings of sedition. There are people,
      you would gather, who think that Stupendic is too powerful
      for the public good. The criticism you would hear on
      all sides is that the Stupendic does not do enough for the
      public welfare. It exists to make a profit for its shareholders
      and even more for its directors. It takes too much
      and contributes too little. It is a soulless machine doing
      nothing for the nation as a whole. Yet the Stupendic has
      the advantage—whatever its other faults—of being supernational.
      It is among the forces which work blindly for
      improved relationships between one country and another.
      This is a matter not of deliberate policy but of habitual
      outlook; the outlook of men whose business is worldwide.
      The model for supernational affairs is not to be found so
      much at Geneva or the Hague as in the boardroom of an
      Oil Company. The larger companies are not among the
      groups which clamour against war; and this is extremely
      fortunate, for conflict widens (as we all realize) in direct
      proportion to the demand for peace. But they do represent
      the international outlook as found among scientists,
      bankers and circus clowns. They are thus in several respects
      a power for good, having long since solved the sort
      of problem which the world's foreign ministers are still
      struggling to define.
    </p>
    
    <p>
      The feeling must linger, however, that a disease is there.
      Among the employees you will note a trace of anxiety, an
      eagerness to justify themselves, an urge to liberalize the
      Company's policy. The period of ruthless competition has
      passed. The battle for supremacy is over. With prosperity
      assured, the time has come for high-minded patronage and
      public spirit. It is the company's aim to serve the people,
      or better still to serve mankind. One aspect of this growing
      liberality is represented by the professionalism of management.
      The manager of to-day adds to his professional
      training an atmosphere of professional etiquette. 'Is this
      good business?' may be his first question, and 'What is the
      tax angle?' his second, but 'Is it ethical?' is now the third.
      In times past the loftiest references to 'service' were the
      invariable preliminary to someone being swindled. This
      is no longer true. The ethics are often as ethical, or very
      nearly as ethical, as they sound. The business man takes
      his place alongside the judge, the preacher and the
      surgeon, being prevented by his cloth from doing this or
      that. To buy at the lowest price and sell at the highest
      is no longer the object in view. He asks only to serve
      the public to the best of his ability, showing a helpful
      benevolence towards his trade rivals (now few and unimportant),
      a generous paternalism towards the prime
      producers, a candid integrity towards the buying public.
      He is among the latest aspirants to professional status,
      full of management jargon to prove his special knowledge,
      full of Rotary Club idealism to prove his essential worth.
      The directors of Stupendic are among the most ethical of
      these very ethical men. The Stupendic contribution to
      many a cause has left the charitable organization prostrate,
      its officials unemployed and aimless. There is a
      scale of generosity that can practically kill.
    </p>
    
    <p>
      Yet even if the Stupendic has a liberal outlook; is a good
      influence, far-sighted and generous; is internationally
      minded and is guilty of none of the crimes of which it is
      accused; <i>it is still too big</i>. The process of integration,
      rationalization and absorption which brought the Stupendic into
      existence may seem inevitable in retrospect (and probably
      was), but the result is something too ponderous to survive.
      Like the vanished dinosaurs the Stupendic has become too
      cumbersome to adjust itself to change.
    </p>
    
    <p>
      The complexity of the Stupendic Gigantsome is apparent from its
      organization chart, its hierarchy, its rigidity,
      its uniformity. And what people fail to realize is that the
      complexities derive not from policy but merely from size.
      Given the mere numbers and distances involved, the
      complex organization becomes inevitable. Decisions become
      impersonal and distant, attributable not to a person
      but to 'Them', It may be the Board's action but on whose
      advice? With complexity comes the need for rules and
      precedents. With it comes the tide of paperwork, the statistics
      and returns. With it, finally, comes the effort to be
      alike. There are people, it is true, who have taken fright
      about the organization man. There are industrial leaders
      who would like to reverse the trend. But this uniformity
      comes not from fashion but from the nature of things. If
      personnel are to be interchangeable, they must be alike.
      They must be prefabricated to a standard pattern. How
      else can the system work? The pattern is complicated
      enough as it is. To introduce the variables associated with
      personality would make it impossible. In light fiction it is
      assumed that a retired British Colonel must conform to an
      accepted pattern (Dammit! What?) just as Generals must
      be choleric and professors absent-minded. Part of the old
      army officer's usefulness lay in the fact that commanders
      could be replaced without any perceptible change in outlook,
      discipline or routine. So it is with the executive sent
      to Honolulu by the Intercontinental Oil Company. Arriving as
      successor in oilsmanship to someone whose training
      is identical, he carries on smoothly from the point where
      his predecessor left off. The replacement of one peppery
      and whiskered Colonel by another, the succession of one
      'oilminded' executive by the next, these are the sort of
      changes observable in the Palace forecourt where the
      sentries may be relieved but the colourful ritual remains
      the same.
    </p>
    
    <p>
      Most of the Giant Company's complexity is the result
      of size. But some of its complexity is the result of age. Few
      of the big Companies are still under their founder's presidency.
      By this stage one of two things has happened.
      Either a grandson of the founder is Chairman, with a
      dynastic title like Rothsfeller III or else the management
      has passed into the hands of the experts. In either event
      the initial momentum will have been lost. Rothsfeller II
      lacks, as a rule, the ruthless virility of Rothsfeller I. He
      lacks the original propellant—the urge to escape from
      poverty or social disadvantage. He offers, instead, the
      picture of authority, dignity, culture and charm. For governing
      an established empire—one he could never have
      acquired—the Third in line can be very well fitted indeed.
      But what about the fourth? As in all monarchies, the
      moment comes when the successor to the throne is a weakling, an
      intellectual, a sportsman or aesthete. Control
      passes inevitably to the experts, the efficiency men, the
      figureheads of the managerial revolution.
    </p>
    
    <p>
      A figurehead, in this modern sense, is a head full of
      figures. The present Managing Director of Stupendic rose
      to that high office by application, knowledge, loyalty,
      foresight, hardness, persistence and luck. The one quality
      he never had or wanted was the quality of leadership.
      Some of his rivals had it and were long since discarded as
      insubordinate. For running the organization he had all
      that he needed: ability, health, versatility and strength.
    </p>
    
    <p>
      But now his labours have been brought to an end.
      And however he goes, whether through illness, accident or
      age, you will be chosen to succeed him; provided you have
      so far followed the author's advice. How does it come
      about? Very simply. The most influential member of the
      Board will use 'the old boy net', telephoning a leading
      figure in the financial world. After each name (other than
      yours) there will be just that pause on the end of the wire
      that will tell him all that he needs to know. Your name
      need never be mentioned. But the thing is done and the
      post is yours. After a lifetime of effort, you have arrived.
      And beneath the shining efficiency of your deep-carpeted
      office you will sense, if alert, the signs of woodworm and
      dry rot, fungus and rust. You will hear the death watch
      beetle's stealthy approach. Are there too many, you
      wonder, on the Board of Directors? Are the initials too
      numerous on each invoice and bill? Is everyone too
      specialized and secretive? Is there too wide a distribution
      of documents and memoranda? Is the prevailing complacency enough
      to suffocate? Is the hearse actually at the
      door?
    </p>
    
    <p>
      What confronts you is not disorganization but decay.
      Decadence is something, therefore, which you need to
      understand. You may associate decadence in your mind
      with black satin pyjamas and absinthe, remarking (and
      correctly) that Stupendic is comparatively free from both.
      But that is to misunderstand the nature of decay. When
      a tree decays it is not normally from sickness and never
      (one assumes) from sin. It decays because it has reached
      its maximum growth, maintaining that size and weight
      for the period usual with that type of tree. It cannot live
      for ever in any case, and institutions, whether political
      or industrial, are not essentially different. For them too,
      maturity leads to decay.
    </p>

    <img src="ch12.0182.svg" alt="" />
    
    <p>
      So it is with Stupendic Gigantsome. Its growth is
      finished. It could, in theory, absorb its chief rival in a final
      merger. This, however, may be prevented by law. Nor
      would it always seem advisable even as a matter of policy.
      The better plan is often to preserve the appearance of
      competition while discreetly agreeing on prices, wages and
      quality. So the Stupendic will retain its present share of
      the trade, expanding one branch and curtailing another,
      introducing automation where practicable and changing
      the angle of its advertising policy from time to time. It
      would seem to be one of the more permanent features of
      the industrial landscape. Its decay, nevertheless, has
      begun, its progress showing more dignity than vigour.
    </p>
    
    <p>
      Why is this? Stupendic has fallen a victim to Parkinson's
      Third Law: <i>Expansion means complexity, and complexity decay.</i>
      Signs of decay are everywhere. Look afresh at the buildings.
      First, there is the original shed of 1912 in which the
      enterprise began, now a museum. Next there is the factory
      building of 1925, vast, sprawling, and impatiently thrown
      together; a mere roof over the machines. Then you have
      the Head Office of 1934, built after the great merger and
      replete with marble and bronze, wrought iron and oak.
      Across the highway are the frantic extensions of 1944,
      fulfilling the purpose of all temporary buildings, which is
      to occupy for fifty years a site that is wanted for something
      else. Finally, there is the latest structure, built in 1960 to
      house the newer departments; Public Relations, Personnel
      Management, Cultural Activities, Welfare and Guidance,
      Recreation and Sport. It is built on a frame of light alloy
      and constructed of compressed wood, fibreglass, polythene
      and paper. What the visitor takes to be the result of a
      burst pipe is the Japanese Garden. What look like chalkmarks on
      the walls are the murals by Sakuma Musashi.
      The premises are notable not only for their lightweight
      structure but for their woodwool insulation and freely
      circulating hot air. Designed by Professor Schnitzelbaum
      of the Wotchester University School of Architecture,
      they represent the latest trends in structure and outlook.
      What is manifest about them is that they will not last
      for long and that they are unrelated to the factory's
      purpose. Whether the Stupendic Group will flourish for
      another twenty years may still be an open question but the
      builders of its latest extension did not give it more than a
      decade.
    </p>
    
    <img src="ch12.0183.svg" alt="" />

    <p>
      Having looked at the buildings, send for the salaries
      list and see what value the Corporation sets on enterprise.
      Executives are broadly of two kinds, those technically
      capable of starting something new and those merely able
      to administer the organization that exists. Which is the
      more important—a new product or a smooth procedure?
      There is usually some lip-service to innovation and progress but
      the real scale of values is expressed in the salary
      cheques. Who matters more, the engineer or the accountant, the
      chemist or the clerk? Did the Works Manager
      gain office by discovering the possibilities of a by-product
      or by running a department without friction? Both types
      of ability may be valued but which is valued more? Where
      the highest value is placed on routine competence, the
      process of decay has begun.
    </p>
    
    <p>
      Last of these preliminaries, visit the most remote outpost of the
      Stupendic Empire, the experimental farm in
      Iceland or the research unit in Tasmania. Discover what
      the scientists are doing and then ask them the crucial
      question: when were you last visited by a director of the
      firm? If the answer is 'Last year' the situation is bad. If the
      answer is 'In 1958' the situation is worse. If the answer is
      'Never' the situation is almost beyond remedy. For while
      decay at the centre may take the form of fussy interference,
      this is consistent with a neglect of things more distant.
      The running down of the central machine will be manifest
      first in the peripheral areas, the places to which central
      authority can barely extend. It is at the farthest of these—on
      the Hadrian's Wall, as it were, of a declining Empire—that
      the breakdown is most obvious. That the cohorts
      there are under strength is less significant than the fact
      that no one has come to inspect them. The Empire may
      still exist but its energy is dwindling and will presently
      vanish. Where this is happening the process of decay is
      well advanced.
    </p>

    <img src="ch12.0185.svg" alt="" />
    
    <p>
      What is the remedy to be? The answer is contained in
      the one word 'Leadership'. And what is leadership, what
      is this secret which each generation must discover afresh?
    </p>
    
    <p>
      It is the art of so indicating a distant goal as to make all
      else seem trivial. When the natural leader has finished
      describing the Holy Grail, the Eternal City, the Glory of
      France or the Honour of the Regiment, all immediate
      privations and perils are thought irrelevant. It was with
      something of the same fervour that old Ben Bandersnatch
      called his men about him on the eve of the first great
      merger. 'Boys,' he said, 'we must think big. If this deal
      goes through, we shall control a quarter of the industry!'
      Who, in the light of his enthusiasm, could have asked for
      a salary raise? Who on the assembly floor could have
      begun discussing a thirty-hour week? Who complained,
      for that matter, if kept at the office all night? It was reward
      sufficient to come home, white and haggard, vowing your
      wife to secrecy. 'Don't tell anyone, Susan, but Old Ben
      is on the warpath. I think he's brought it off. The news
      should be all over the world on Thursday. Gosh, I'm worn
      out!' Assume, if you like, that Susan's husband had nothing to do
      with the deal. Suspect, if you will, that he was
      not even wanted at the office. The fact remains that he
      was living in the presence of a drama besides which his
      own affairs could be forgotten. This was the mood in
      which men fought at Austerlitz, Trafalgar or the Normandy Beachhead.
      It is under an inspired leader that the
      soldier comes to regard his own possible death as a mere
      incident. It is under an industrial leader that the workman can
      grudge his own wage as a debit figure in the
      firm's tottering finances. At least in the early and adventurous
      phase of an industry, the excitement can mean
      more to people than the pay. 'In those days,' they can
      boast afterwards, 'Men were <i>Men</i>.' But those days of
      adventure are long ago. The mere size of the organization
      has created complexity, and with complexity has come
      the rule of gravity; so weightily expressed in ponderous
      speech, in set ways, in certain mental attitudes, above all
      in argument. And gravity has won.
    </p>
    
    <p>
      The situation is one which might be compared,
      superficially, with that faced by a hotel proprietor who
      has returned, after a long absence, to find the hotel
      neglected. The rooms are dirty and the paintwork has
      suffered, the service is bad and the food is worse. Servants
      who are perfectly capable of good work have picked up
      idle and slovenly habits. There are several possible
      remedies but the quickest and most effective is to announce
      a cocktail party for two hundred guests, to be followed
      (after three days) by a Banquet and (two days later) by a
      Ball. Cooks, waiters and barmen suddenly find themselves faced
      by a major crisis, with problems as numerous
      as they seem insoluble. Their morale rises overnight and
      the hotel becomes a different place. Any other organization can
      be at least temporarily revived in the same way.
    </p>
    
    <p>
      Can you re-create, by such means, the sense of adventure and
      hazard? Can you rally Stupendic to face the
      eventual competition of people elsewhere who work instead of
      argue, of people to whom progress means more
      than just a longer weekend? This you can do, provided
      only that you have remained, by some miracle, young at
      heart. For success goes to the young and youth is the
      knowledge that there are new worlds to conquer and that
      there is always time for change. No matter how old you
      are, youth is a possession you can keep if you choose. It
      can be there for all to see—in the spring of your step, in
      your quick readjustment to changed circumstances, in
      your willingness to take a risk, in your readiness to laugh.
      Those who know what decay is and are prepared for its
      onset can be free, for their lifetime, from Parkinson's
      Third Law. Over them gravity has no hold. Not for them—and not
      for you—that solemn adherence to the Company's pattern,
      that burden of complaint, that weary
      dignity of age. You can take things lightly if you will,
      obeying rather the rule of levity: the rule that has landed
      you, and will keep you, at the top.
    </p>
    
    <p>
      But... no organization can last for ever. Even while
      you are displaying all your gifts of leadership and saving
      Stupendic in the nick of time, another and a younger man
      has his foot in the door of another and more virile organization.
      His present desk is his first. He almost seems to live
      at the office. His career has just begun.
    </p>

  </body>
</html>
