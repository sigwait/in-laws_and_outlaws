<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="style.css" type="text/css" />
  </head>
  <body lang="en">

    <h2>Paperwork</h2>

    <p>
      Nearing the summit of your organization, a hierarchy
      in which you rank now perhaps as Number Three,
      you will begin to realize what it means to be at the top.
      You will be in sole charge when others are on holiday.
      You will see at close range how the highest executives work
      and you will come to realize what their difficulties are.
      Chief among their problems, you will find, is one with
      which you have been acquainted from the start; the
      problem of paper. But here, at the very senior level, the
      problem will seem to have acquired a new dimension.
      Paper is no longer a nuisance but has become a nightmare.
      More than that, it is a brooding menace, a seething tide
      which can swamp and drown. On your success in dealing
      with it your next promotion must depend. It is a flood in
      which you must either sink or swim.
    </p>
    
    <p>
      This flood of paper which now threatens to submerge
      the world is something peculiar to this century. The
      Hellenistic scribes who wrote on papyrus, the Chinese
      bureaucrats who exercised their penmanship on silk, and even
      the 18<sup>th</sup>-century clerks who inscribed their civilities on
      rag paper with a quill pen, were guiltless of anything
      that could be called mass production. It is our own age that
      has developed the swiftness of communication, the abundance
      of paper, the multiplication of copies and the
      widespread semi-literacy which are the immediate causes
      of the paper flood. Ease of communication has also made
      practicable such a degree of centralized control as was
      never known before. Until a century ago every largescale
      and scattered organization or empire was engaged in
      a ceaseless struggle to make its distant units conform with
      central policy. For reference to headquarters there was
      neither the inclination nor the time. Those in positions of
      theoretical responsibility read with helpless dismay of
      provinces annexed, officials sacked, branches opened and
      ships sold, their bleating protests coming perpetually too
      late. With the laying of transoceanic cables they felt for
      the first time that they had their agents on the leash.
      From about 1875 began, therefore, that tightening of the
      chain that has finally destroyed the effectiveness of (among
      other things) colonialism and diplomacy. The professional
      bargaining of plenipotentiaries has given place to
      the bickering of impotent office boys, each tied to the
      apron-strings of a government which has never even heard
      the persuasions of the other side. Diplomatically,
      administratively, commercially, the process of centralization has
      been carried to its logical conclusion with all authority
      vested too often in a single man; and he, from overwork,
      quite obviously off his head.
    </p>
    
    <p>
      It was inevitable that the central administration would
      make full use of the tools that had suddenly become available.
      After centuries of frustration those in authority
      could at last impose their policy upon the whole organization,
      not merely from day to day but from hour to hour.
      They could exact the fullest information, collate the most
      detailed returns, draw up the most voluminous directives
      and issue the most peremptory commands. Of these opportunities
      they have made the fullest use. But for all this
      they have had to pay the price. The penalty has been
      that correspondence pours on them in the present flood.
      Surrounding themselves with executives, they battle with
      a rising tide of paper. Ordinarily waist-deep in letters and
      memoranda, a week's illness will bring the high-water
      mark up to their chin. Rather than drown, the key man
      prefers to suppress his symptoms and stay at his desk; often
      with the worst results for all concerned.
    </p>
    
    <p>
      After more than fifty years of tightening control, with
      all initiative killed at the circumference and all leisure
      abolished at the top, some people have began to ask
      whether much that is now technically possible is, always
      and everywhere, practically wise. Some newly formed
      commercial empires, the results of mergers, have preferred
      to avoid centralization except for purposes of raising
      capital. In others, already centralized, the question is
      being asked whether control from the centre may not have
      gone too far. In a few, a very few, the machine has been
      actually put into reverse. One chain store organization,
      for example, has totally revised its system of control.
      Merely by deciding that the head office should trust the
      branch manager, that the branch manager should trust
      the girls behind the counter, and that the girls should
      trust the customers, this organization dispensed with time
      cards, complaint reports and stockroom forms. It was
      agreed to assume that all concerned are honest; partly
      because they are and partly because it would still be
      cheaper even if they were not. In thus eliminating
      22,000,000 pieces of paper per year, weighing 105 tons, all
      that the directors lost was a mass of statistical information
      of which no use, in fact, had ever been made. One result
      was an immediate reduction of staff. Another result has
      been the flattering interest shown by other firms. Here is
      one Board that has escaped, somehow, from the toils.
    </p>
    
    <p>
      In this particular instance the time for reducing paperwork
      began, significantly, when the Chairman of the
      Board was visiting a branch store on a Saturday afternoon.
      Finding all the girls working overtime to complete the
      catalogue cards he asked what the cards were for. 'For?'
      repeated the supervisor blankly, 'They are for filling in.
      Here is one, sir. You can see for yourself that it has to be
      filled in.' 'But why?' asked the chairman. No one could
      tell him. No one knew. No one had ever known. It was
      from that moment that the paper-saving movement began.
      What we have to realize is that nothing would have begun
      had the chairman stayed in his office, holding committee
      meetings and answering mail. He was not only visiting
      a minor branch store when his idea dawned—he was
      visiting it on a Saturday afternoon. This may serve to
      emphasize the basic lesson, that the highest executives can
      save no others until they have saved themselves. The man
      whose life is devoted to paperwork has lost the initiative.
      He is dealing with things that are brought to his notice,
      having ceased to notice anything for himself. He has been
      essentially defeated by his job.
    </p>
    
    <p>
      To illustrate this point, let us compare the daily routine
      of two Managing Directors, whose names, purely imaginary,
      are Pending and Leederman. Pending arrives at the
      office to find his in-tray piled to the height of 18¾". Of this
      stack 11½" comprise the information that Pending has
      demanded, and the remainder consists of files on which
      his decision is asked. Hardly has he looked at the first item
      before the day's mail is added. Of this additional 8", a
      fair proportion consists of circulars from the Board of
      Trade. For paper of this kind every office has a suitable
      receptacle, but the rest of the mail needs answering. The
      process of dictation has scarcely begun when the first
      telephone call is received. With a staff conference at 11.30,
      lasting nearly an hour, and with continual interruptions
      of every kind, Pending fights back against the tide of
      paper. For a time it may seem that the flood is gaining on
      him. With a supreme effort, however, he masters it. The
      level begins to fall. With everything thrown into the battle,
      with blood, sweat and tears, Pending disposes of all the
      business on hand. By the time he leaves the office every
      letter has been answered, every problem solved and the
      in-tray left empty. 'I have won again,' he reflects as he
      goes wearily to his car. He has earned his salary and
      doubts whether anyone else could have done as much.
      But what will happen if he goes sick? He can just cope
      if all goes well. But what if it doesn't? Some day it won't.
    </p>

    <img src="ch09.0129.svg" alt="" />
    
    <p>
      Mr Leederman has an outlook which is totally different. For
      him the flood of correspondence is merely an
      interruption. Were it to occupy the day, as happens with
      Pending, Leederman would think that his time had been
      wasted. This outlook is reflected in his routine. His mail
      is opened at 8.45 and the rule of the office is that fifty
      letters must be dealt with by 9.15. Leederman often replies
      in his own hand-writing, usually at the foot of the document
      he has received. His answers are laconic: 'Sorry—can't be done,'
      'O.K. I'll be there,' 'I quite agree,' and,
      occasionally, 'DRIVEL! To other letters he dictates a
      brief reply. Brief it must be, for those not answered by
      9.15 will have to be answered by someone else. That is the
      moment at which his dictation stops. At 9.20 there is a
      staff meeting which lasts no more than ten minutes and at
      which outstanding problems are dealt with verbally. The
      meeting over, Leederman grabs the telephone and attempts
      to make eight long-distance calls before 9.40. He has
      found by experience that the lines are more likely to be
      free then and that the same calls, made later in the morning,
      would take twice as long. At 10.0 he quits his desk
      and leaves the office, beginning a leisurely tour of the
      factory or setting off on a visit to a branch establishment.
      Instead of tearing round the factory so as to get back to
      the office, he tears through the office work in order to have
      time for the factory. He greets the elderly foreman, 'Hallo,
      Fred—how's it going?' 'Tell me, Bill, how's that new lad
      shaping on No. 5?' He feels pipes to see whether they are
      too hot. He notices a light left on in daylight. He sees a
      blocked gutter and tells someone to clear it. He asks after
      a man's wife, whom he knows to have been ill. And all the
      time he is noticing things and talking, his mind is revolving
      round a new idea. How would it be if they installed a gantry
      in No. 11 shed, making a new loading point
      at the back? Pondering this problem, he spots a boy he has
      never seen before. 'Who is the ginger-haired lad over
      there?' Later that day the awe-stricken youth will be telling
      his mother 'The Boss actually spoke to me, asked me which
      school I had been at—just like as if he was interested!' If
      there is any trouble in Leederman's factory—and there
      seldom is—he sees it coming from a mile off. If the place
      were to catch fire, he would be the first man on the spot;
      a fact which everyone seems to have realized.
    </p>
    
    <p>
      It is just the same at the branch establishments. Leederman
      always tells them when he is coming. Why? Because,
      he says, the value of the visit lies half in the tidying-up
      process which precedes it—the lick of paint on the previous day.
      He would have achieved something even if the
      visit had to be cancelled at the last moment. But his visits
      are never cancelled and never even hurried. He sees
      everything, even the new tennis court. He has lunch in the
      staff canteen, taking care to meet everybody beforehand
      in the club-house. If there is a lack of leadership, a decline
      in loyalty, that is the time when he detects it. His theory is
      that the strained relationships are obvious from the way
      people group themselves. When he sees the branch manager at
      one end of the room, surrounded by a group of
      executives, while the rest are grouped at the other end of
      the room, surrounding a senior departmental head, he
      senses at once that there is something wrong. What precisely
      is wrong he may not immediately guess but he
      rarely leaves the place before he has found out. Should
      he stay the night, as he often does, the head office staff
      know that he will ring up at exactly 9.20 next morning.
      By then his mail will have been opened and his deputy
      will have framed fifteen or twenty questions, to each one
      of which the answer can be 'Yes' or 'No'. After five or
      six minutes Leederman will ring off and turn once more
      to the things that matter. There are people who believe
      that he is a very good manager indeed.
    </p>
    
    <p>
      The essence of Leederman's philosophy is that the good
      manager retains the initiative. He does not allow himself
      to be penned into his office by a flood of routine business.
      He anticipates the questions before they have been put on
      paper. He foresees the difficulties before they have turned
      themselves into memoranda. He has gone out to meet the
      trouble before it has really begun. Towards thus gaining
      the initiative a useful first step is to rid oneself of a common
      misconception about what matters most. To illustrate this
      misconception, picture Mr Tangible (Sales Manager of
      Steeply, Rising, Ltd) calling on Mr Phonewright, Managing Director
      of the Longworthy-Faroff Group.
    </p>

    <p><br/></p>

    <p>
      PHONERIGHT: 'Come in, Mr Tangible!' Sit down. I'm
      glad to see you. Smoke? Do forgive me for one moment
      while I sign these letters ... (<i>pause, scribbling</i>) ... And I
      have to answer this message from the Works' Engineer—sorry!
      ... (<i>pause, more scribbling. Telephone rings</i>) That you
      Henry? Oh, good! That's fine! I'll see your new plant
      when I get the chance—don't get much time though.
      What's that? Maybe you could put that in writing. Yes,
      I see that. But what about the overheads? No, it's not that
      simple, Henry. There's the accountants to satisfy and the
      Board to convince. Phone back later when you have the
      figures I asked for. O.K., Henry. That will be fine. G'bye.
      ... Sorry about this, Mr Tangible. Where was I? Yes, I
      had this message to answer ... (<i>pause, scribbling. Telephone
      rings</i>) ... Bob! I was just going to ring you. Yes, yes, I
      heard about that. Too bad, too bad. Has she recovered?
      That's good. Look, Bob, I've thought again about what we
      were discussing yesterday. I think it can be managed, but
      whoever we put in charge will have to be good. Who?
      No, Bob, Joe couldn't do it. Besides, he will be on another
      assignment. Think it over! G'bye. ... (<i>pause, scribbling.
        Telephone rings</i>) ... That you, Peter? Golf next Sunday?
      Good idea. Shall I ask Roger? O.K. then. Who'll be the
      fourth man? ... But, look, Peter, he's not quite in our
      class, is he? What's his handicap? ... I suppose he must
      have improved, but still... certainly, I know that!...
      Peter, I have another idea. Suppose we ask Neville? ...
      etc., etc., etc. (<i>Tangible goes quietly out of the room and
      borrows a telephone at the Commissionaire's desk.</i>) ... 'All
      right then, Peter—I'll see you Sunday. How's the family?
      Splendid! Where were you going on holiday this year? ...
      But you went there last year! Well, that's true .. . Oh, we
      were thinking of going to Spain... No, Sheila never
      went before...O.K. then. See you on Sunday!...
      (<i>pause. Phonewright looks vaguely around. Wasn't there
        somebody waiting to see him? Odd! Oh, well. He scribbles afresh.
      Telephone rings.</i>) 'Who's that? Oh, Mr Tangible? Good to
      hear your voice! Yes, we were very interested indeed in
      the heavy equipment you have to offer... . Yes, yes... .
      Right then, we'll start with two of them, on trial... .
      Yes, it will be a large order if we are satisfied ... . Fifty,
      at least, and maybe a hundred by the end of the year.
      What would the price be on fifty or more? ... Come,
      you could give us a better discount than that! ... Oh, I
      know ... Well, send two of them and we'll see. G'bye.'
    </p>
        
    <p><br/></p>

    <p>
      The misconception here is that everything should take
      priority over the man who is actually present. This would
      make sense if Phonewright were deliberately showing
      Tangible where he gets off. It would be a reasonable
      method of indicating that Steeply, Rising Ltd is an organization
      of no great repute or importance. But that is not
      Phonewright's motive. He is merely working on the
      assumption that a piece of paper is more important than
      a caller and a telephone more important than either. It
      is not that he regards Henry, Bob and Peter as top-ranking in
      themselves. He simply gives priority to a telephone
      as such, a fact which his colleagues have realized. It
      involves, as a habit, the risk of misjudging character and
      the certainty of losing business.
    </p>
    
    <p>
      Where paper is concerned, public administration has
      had a bad effect on business. For while the civil servant's
      methods may be similar, his aims are different. For him,
      the file is an end in itself. Why? Because there is always the
      possibility of a public inquiry. At any stage of his career,
      questions may be asked as affecting his work. What action
      did he take in response to this? Was he the man responsible for
      recommending that? Whom did he consult before
      rejecting the other? So what the civil servant needs to
      protect himselfis a file recording exactly what he has done.
      On receiving the application from A he laid it before his
      next superior B, having first obtained a legal opinion from
      C, which came to be embodied in Minute 43, dated March
      27<sup>th</sup>. Advised by B, D then took action as follows....
      The civil servant wants to show that he took the right
      decision, gave the right advice, asked the right questions
      and obtained the right facts before placing the right
      minute before the right authority. What actually <i>happens</i>
      is of little consequence. It is the file that has to be in order,
      not the people or things to which the file relates. There is
      a riot, we will suppose, at the prison, with two warders
      killed and five injured, the carpentry shop burnt and ten
      convicts at large. When the telegram arrives the official's
      concern is more with the file than the prison. What action
      would look best for the record? How best to ensure that
      the blame is not fixed on the official's department? What
      form of inquiry will produce the most soothing report?
      This attitude may be inevitable among civil servants but
      it is unhelpful in business. For there the distribution of
      blame does not lessen the fact that the large order has
      been cancelled, the client lost, the contract awarded to
      another firm and the goods not up to sample. In business
      the concern should not be with the file as such but with the
      people and the things. The Branch Manager may besacked
      for showing too small a yield on the capital outlay, the
      correctness of his procedure being no excuse. The neatness
      of the file will console nobody, in fact, for the losses
      incurred.
    </p>
    
    <p>
      What is to be done about this paper flood? It is no
      answer to prefer the telephone to the typewriter for while
      both consume time, the former does not even record what
      has been agreed. It is no answer to hold frequent conferences,
      with everyone flown there and back. Root cause
      of the paper torrent is the urge to over-centralize, an urge
      which exists in the nature of things. For centralization, up
      to a point, is inevitable. In so far as a chain store enjoys
      an advantage over a small family retail business, its
      greater efficiency must derive mainly from a centralized
      system of purchase and distribution. That there must be
      some control is manifest. The problem is to decide how
      far it should go and at what point it should be relaxed.
      Where should central policy give place to local initiative?
      No complete and final answer to this question is possible.
      If we are seeking criteria, however, by which to judge the
      point at which centralization should stop, one criterion
      might be the size of the head office staff in relation to the
      total numbers employed. Of the relevant facts this is at
      any rate one.
    </p>
    
    <p>
      There is a strange and general reluctance to fix a
      normal ratio between administrative and other costs.
      Conceding, as we must, that businesses differ and that
      departures from the ideal are bound to occur, it would
      still seem possible to discover an acceptably economic
      ratio, departures from which could at least be explained.
      When challenged, some business men will mention, uncertainly,
      a tentative figure of 15%; more as a maximum,
      perhaps, than as an ideal. British Universities spend between
      10% and 6% on administration. There are allegedly
      fighting services in which administrative expenditure
      seems to dwarf all other types of expense. There is a distinction
      to be drawn, however, between the percentage
      spent on administration as a whole, and the proportion of
      that which goes to the head office. For the first and larger
      percentage is an indication of general efficiency (or
      incompetence), the second a very rough measure of the
      degree in which the organization is centralized. In one
      very competent firm the head office staff comprises 2.34%
      of the total employed, the proportionate expense being
      somewhat higher. There may well be a number of efficient
      firms in which about 10% goes on administration and
      half that on the central office. In general, the greater the
      central demand for statistics, returns and reports, the
      larger must be the staff needed if only for filing them. If
      they are to be digested and analysed, that means more
      staff and any consequent action (whether by way of praise
      or reproof) will mean more staff again. As collection,
      analysis, action and follow-up all represent successive
      degrees of control, the size and cost of the head office staff
      would seem to indicate, roughly, how much control there
      is. Where there is too much, it will probably cost too much.
      Were a perfect ratio to be established it would not be perfect
      in every case. There is no reason to suppose that what
      is perfect for a retail store chain would be perfect for (say)
      the National Coal Board. The fact remains, however, that
      a wide comparison of staff ratios would at least provide
      some food for thought. If the ideal solution were not apparent,
      we might at least remark some departures from the
      norm.
    </p>
    
    <p>
      While a high proportion of the paper in circulation
      represents an over-centralized control, a rising proportion
      reflects nothing more than an urge to circulate. With
      modern duplicating methods it is as easy to provide fifty
      copies as ten. This being so, the human tendency is to
      order the larger number. Should extra copies be wanted,
      it is good to have them ready and relatively inexpensive
      to have them made. It is another human tendency, however,
      to distribute copies you have. It saves cupboard
      space for one thing. It also prevents people complaining
      afterwards that they were never told. So the distribution
      list is planted, one might say, in a fertile soil. It tends to
      lengthen and expand, blossom and ramify. Copies must
      go to all executives, all foremen, all supervisors and consultants.
      There must be copies for all specialists in
      economics, statistics, welfare, psychology, method, publicity,
      training and youth. There must be copies for
      the lawyer, the doctor, the accountant and dentist, the
      detective and scoutmaster, storeman and nurse. Copies
      must be placed on all notice-boards, in all recreation
      rooms, in every lavatory and on every door. All, but all
      must be informed. One result of this wide distribution is
      to be measured in the inches of paper on every desk.
      Another is that nobody reads what everyone gets. Nor
      would anyone who did have time for anything else.
    </p>
    
    <p>
      The urge to circulate, which is prevalent in business,
      and endemic in government, reaches its worst excesses in
      the field of scientific learning. In research establishments,
      for example, the greatest efforts are concentrated on
      keeping the scientists in touch with each other's work and
      progress. Whole departments are devoted to this, producing a
      copious flood of printed and mimeographed
      memoranda. When a research department grows beyond
      a certain size we are told that the need for internal
      coordination overshadows the need for actual research.
      Perfect co-ordination is achieved only when there is nothing
      to co-ordinate. This internal problem might be regarded
      as serious; and so no doubt it is. Dwarfing it, however, is
      the problem posed by the scientific journal. For the tendency
      of learned periodicals to multiply has a bearing on
      the assumption that all periodicals should be read. Why,
      to begin with, should they multiply? Because each must
      fall, sooner or later, into the clutches of a professor (A)
      more fanatically jealous than the average. Under his
      editorship no article is accepted with which he does not
      agree and no book kindly reviewed other than those
      written by his own former pupils. The rival professor (B)
      whose articles have been most consistently excluded will
      then, and inevitably, start another journal; one edited at
      first on more liberal principles. B will accept articles from
      all who are not actual and known adherents of A. He
      eventually draws the line however, at contributions from
      C, whose works are confused, long, and original only in
      their grammar and punctuation. But C realizes by now
      where his remedy lies. He becomes the founder of a new
      and less hidebound periodical; one more open at first to
      new and confused ideas. There is a difficulty, in the end,
      however, over the articles submitted by D, who cannot
      even spell. But D is not to be denied access to the misprinted
      page. He hesitates, to be sure, before adding one
      more journal to the library shelves; but not for long. His
      duty is clear and he does not shirk it. And so the process
      continues until there are eighty journals or more in dentistry
      alone. As for the whole field of learning, the figures
      are staggering, and not the least so in their rate of increase.
      A University library may take up to 33,000 periodicals.
      Each learned journal will have a council, an editor, subeditors
      and staff. Each must involve a great deal of work.
      And the final result, as seems sufficiently well known, is
      that the few scientists who matter exchange their ideas in
      private correspondence. It has also been argued that the
      multiplication of journals is in inverse proportion to the
      progress made. With less time wasted on editing and reading
      there would be more time, possibly, in which to work
      and think.
    </p>
    
    <p>
      If scientific journals tend to multiply, trade and technical
      publications certainly become no fewer. On to the
      desk of the senior executive there pours a torrent of paper
      and he will be judged at first on his ability to deal with it.
      This can be initialled as seen. This other must be referred
      to a higher level. Here is one that must be answered and
      here another that can be ignored. Refer this back as incomplete
      and the next as incorrect, say 'Yes' to this and
      'No' to that, expedite one and lose the other. Have that
      filed and this destroyed, mark these as urgent and let those
      wait. Retype this as corrected and make a draft reply to
      that. Check by telephone whether the reply means what
      it says and confirm verbally that you say what you
      mean. Total the figures again and compare them with the
      estimate. Verify the spelling of Vaaderschnelling's name
      and decipher, if possible, his Works Manager's signature.
      Ask White to call to-morrow and tell Black to chase himself.
      Thank Brown for his help and tell Green to pull
      himself together. Give Sylvia some typing to do as she
      looks idle and send Jean home because she looks ill. Don't
      let the paper mount high in the in-tray. Don't let the
      pending get heaped on the floor. Deal with the paper,
      answer and file it, read it and sign it and send for some
      more.
    </p>

  </body>
</html>
