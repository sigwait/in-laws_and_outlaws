<?xml version="1.0" encoding="UTF-8"?>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="style.css" type="text/css" />
  </head>
  <body lang="en">

    <h2>Expertize</h2>

    <p>
      In the course of your business career you will hear
      constantly of experts on Organization and Method. You
      will eventually meet them and wonder, perhaps, whether
      this is a career which you might yourself adopt. You will
      have realized, after all, that doing a job is more difficult
      than telling someone else how the job ought to be done.
      The temptation is a real one but it should be resisted. Let
      there be no misconception about this. Nothing in this
      book should be taken as a reflection on the experts in
      Organization and Method. But that vocation is not for
      you. As against that, you will often have to deal with
      Business Consultants. You do well, therefore, to study
      their habits and make yourself fully acquainted with the
      nature of their business. They have come to form a permanent
      feature of the business landscape, prominent,
      vociferous and inescapable.
    </p>

    <p>
      There are many consultants whose respectability is
      beyond question and many others against whom nothing
      has actually been proved. More than that, the business of
      consultation is on the increase. To the industrial firm the
      expert in method is the equivalent of the psychiatrist and
      the tranquillizer, being called into play when the strain is
      proving too much for its constitution, equilibrium and
      nerves. Just as the troubles confided to the psychiatrist are
      lessened by being shared, as also by the realization that
      they are far from unique, so the feverish pulse of a large
      organization will slow down when the business consultant
      appears and before he has uttered a word of advice. 'Yes,
      yes, yes,' he will murmur soothingly. 'Lights burning late
      in the office, friction over the parking facilities,
      ill-tempered behaviour at lunch; all the well-known
      symptoms—just like a case I attended last week.' But the
      bedside manner
      is not the whole of his stock in trade, nor is sympathy
      all he has to offer. Sooner or later he will give positive
      advice—and that is what leaves us puzzled.
    </p>

    <p>
      In a business organization headed by a Managing Director of
      eminence, run by directors of experience and overflowing with
      executives of proved competence and zeal, it
      must always seem strange that consultants should have to
      be called in from outside to advise on how the work
      should be distributed. If the directors don't know how to
      organize the concern it seems fair to ask what they do
      know and for what, indeed, they are being paid. It is
      also natural to wonder what reason there can be for
      supposing that outside consultants should know what the
      Board do not. These questions, which have long baffled
      the public at large, would seem to deserve an answer.
    </p>

    <p>
      We can reasonably assume, to begin with, that the
      consultants know very little. Their right to advise derives
      essentially from their initial act in putting up their nameplate:
      'Sneering, Shockwell and Foggarty, Business Consultants', or
      'Sadleigh, Deep, Loring and Muddleworth,
      Efficiency Engineers'. Everything has to have a beginning
      and, for the methods expert, this is it. What were Foggarty
      and Muddleworth doing before they proclaimed
      their expertize? This is by no means apparent. They may
      have taken a correspondence course. They may have
      attended a class at the London School of Economics.
      They may merely have failed to earn their living in any
      other way. But whatever their precise experience has
      been, there they are, experts in expertize, self-proclaimed
      magicians of the business world. Hardly has their bronze
      tablet been screwed to the door-post before the first leader
      of industry is hammering for admission. As the queue forms
      in the corridor the skilled observer will note that the
      gathering crowd of business executives includes few, if any,
      representatives of the government. No General is there,
      pausing in the midst of a campaign and anxious for advice
      as to whether he should withdraw or attack. No Admiral
      has rushed ashore to sob his troubles into a sympathetic
      ear. And if the Prime Minister is among these jamming
      the entry, we must conclude that his disguise, at least,
      is good.
    </p>

    <p>
      The middle-aged men blocking the pavement are drawn
      almost solely from the world of industry and commerce.
      They are qualified, decisive, tough and keen. The Chairman
      of the firm which makes celluloid goldfish knows just
      how his product should be manufactured, advertised and
      sold. The Managing Director of the firm which produces
      plastic balloons knows more, it is said, about elasticity
      and bursting-point than anyone else in that mammoth
      industry. The Sales Manager of Lollipops Ltd is unequalled
      in his grasp of stickiness, timber, sugar content
      and colour preference. There is no one in the crowd who
      does not know his business. And yet they all seek advice,
      as it seems, of Sadleigh, Deep, Loring and Muddleworth.
      Why, for heaven's sake? What does Sadleigh know about
      celluloid? As little as Deep knows of balloons or Loring of
      lollipops. What magic formula is it that these experts have
      to sell?
    </p>

    <p>
      A careful survey has now established the fact that the
      clients who approach a business consulting firm do so with
      one of two motives. On the one hand, they may want
      scapegoats for the reorganization upon which they have
      already decided. On the other, they may want to prevent
      such a reorganization taking place. The contrast between
      these two procedures can best be illustrated by reference
      to two recent case-histories. For obvious reasons the identity
      of both the firms and their Business Consultants will be
      concealed under names which are purely fictitious. Of the
      two Companies concerned, the first we shall call the Horseless
      Carriage Co. Ltd, mass-producers of veteran cars. The
      second we shall call Historic Homes Ltd, mass-producers
      of pre-fabricated homes, each with a pre-fabricated story
      of how Queen Elizabeth I slept there and with whom.
    </p>

    <p>
      The directors of the Horseless Carriage Company of
      Coventry decided recently to streamline their organization.
      They decided, therefore, to sack half their executives
      and demand some real work from the other half. Their
      problem was how to do this without being tarred and
      feathered in the middle of the Company car park. In
      order to avoid this, they agreed that their proposed
      reorganization should be the work of outside consultants, so
      they called in Messrs Sneering, Shockwell and Foggarty
      and explained briefly what advice they expected to receive.
      In such a situation as this, the consultant's chief
      advantage is that he need not linger on the scene. He
      presents his report with one foot in the jet aircraft's door,
      reaching 20,000 feet before anyone has finished reading the
      first paragraph (which consists entirely of thanks to all for
      their co-operation). These particular consultants work as a
      team. Once briefed on what the policy is to be, they go
      swiftly into action. Sneering is condescending in a supersilly
      way—'Is that your <i>latest</i> I.B.M.?' 'You have never
      heard, I suppose, of marginal costing?' and so forth.
      Shockwell is direct and brutal. 'You carry absurdly swollen
      overheads,' he says firmly. 'You should cut down by 52
      per cent.' Foggarty follows up with an involved explanation
      of why the changes are necessary. On this occasion the
      results of their incursion into the business can be tabulated
      as follows:
    </p>

    <ol style="list-style-type: lower-alpha">
      <li>Half the executives were sacked.</li>

      <li>
        A digital computer costing £1,000,000 was acquired
        as a symbol of progress.
      </li>

      <li>All partitions were demolished, making a general
        office out of the space previously occupied by individual offices, and
      </li>

      <li>
        The office colour scheme, which had been primrose
        and white was changed to lilac and grey.
      </li>
    </ol>

    <p>
      Very different (at first sight) was the influence of Sadleigh,
      Deep, Loring and Muddleworth upon the organization of Historic
      Homes, Ltd. The directors of Historic
      Homes had been under pressure from a group of shareholders,
      whose noisy spokesmen, Barker and Maybite,
      insisted that the firm's organization should be modernized.
      What exactly they meant by this proposal was never
      very clear but they did not scruple to suggest that the
      directors were grossly overpaid considering that their
      responsibilities concerned only an unmodernized firm.
      Faced by this dangerous movement, the Chairman called
      in the experts. Their instructions, on this occasion, were to
      report that the organization (except in one or two minor
      respects) was already perfect. In this other group of consultants
      there is a different allocation of work. Sadleigh
      shakes his head, Deep looks profound, Loring specializes
      in a subtle compromise between a roar and a leer, and it is
      Muddleworth who enters into an obscure and involved
      explanation of why the changes are essential or (as in this
      case) needless. As a result of their incursion into the business,
      the following changes were made:
    </p>

    <ol style="list-style-type: lower-alpha">
      <li>
        Messrs Barker and Maybite became Directors.
      </li>
      <li>
        A digital computer costing £1,000,000 was acquired
        as a symbol of progress.
      </li>
      <li>
        The general office was divided up by partitions to
        form individual offices, and
      </li>
      <li>
        The office colour scheme, which had been lilac and
        grey was changed to primrose and white.
      </li>
    </ol>

    <p>
      There would thus seem to be a complete contrast between the
      advice given in these two cases. Such a contrast
      there may be but the point of significance is that, on both
      occasions, the efficiency expert did something which the
      directors could not do for themselves. In the one instance
      they took the blame for a purge which would have been
      impossible for those who had to live afterwards in the
      same neighbourhood. In the other instance their function
      was to assure the shareholders that the whole organization
      had now been modernized (whatever that means)
      and that there could be nothing more to worry about. As
      regards the partitions and paintwork it would be wrong,
      incidentally, to suppose that any of the experts had any
      strong preference either way. Their object was simply to
      demonstrate that something had been done.
    </p>

    <p>
      While the two case-histories just outlined are clearly
      typical, it would be a mistake to assume that the experts
      never have anything more useful to say. No one with
      experience in teaching could make that rash assumption,
      for he must instantly perceive where the consultants have
      another advantage. Every teacher must at some time have
      had the experience of being asked, at short notice, to
      examine in some subject of which he knew practically
      nothing. To the novice such an abrupt request may bring a
      moment of dismay. To the older teacher, it is no more than
      a nuisance, for he knows how the situation should be
      handled. When the scripts arrive he sorts out the half-dozen
      which appear to be legibly, neatly and competently
      done. Reading these, he soon discovers what the right
      answers are supposed to be, at least for the purposes of
      that particular examination. Then he marks down the
      others for giving answers that are different and so presumably
      incorrect. In the same way the efficiency engineer
      has opportunities for comparing one organization with
      another. He guesses which are the best and can then
      criticize the others for not being the same. In this way his
      advice, if it is really wanted—and it very occasionally is—can
      be surprisingly sensible.
    </p>

    <p>
      Biologists tell us that trees and plants are pollinated by
      the bees as they pass from one to another—a process now
      often mechanized but still essentially the same. The efficiency
      expert is the bee of industry, buzzing from one
      industrial plant to the next and pollinating as he goes.
      Many a bee will stoutly maintain, no doubt, that the
      pollen is his own invention, perfected by a secret process
      unknown to the other bees. Such a bee is a liar and so,
      normally, is the consultant who pretends to have ideas of
      his own. The ideas, like the pollen, come from another
      plant. And the intensity of this cross-fertilization is in
      proportion to the number of the bees. Bee-keepers still living
      can recall a period when they had to pay a rent for bet
      pasture. With a fuller knowledge, it is now the orchard
      proprietor who may have to pay the bee-keeper for his
      co-operation. In these circumstances the bee populations
      will increase. Business consultants do the same, the fact
      that they are in demand being proved by the fact that they
      are in such large supply. And there are all sorts of situations
      in which, lacking their help, a business man could
      seek advice only from his competitors. The point may have
      been reached, however, when the experts have become too
      numerous. That would be no proof, however, that bees
      are useless in themselves. Granted that pollination may
      become excessive, it remains true that pollination must be
      done.
    </p>
    <p>
      
      A problem which remains is to decide how the bee
      knows a good flower from a bad. Granted that the business method
      consultant will carry ideas from one organization to another, how
      are we to know that the ideas
      chosen are the best? If the examiners can find what the
      right answers are from a glance at the handwriting, by
      what similar method can the efficiency expert choose any
      one organization as a model for the rest? And our anxiety
      on this score is intensified by our knowledge of what
      happens in school administrations. It is a known fact that
      school inspectors, buzzing from one school to another, collect
      the worst schemes from each and make them compulsory for all.
      How are we to know that business consultants do not do the same?
      It would be unethical to
      reveal all the secrets of the profession, but this is a point on
      which students seem fairly entitled to reassurance. And the
      secret, in this instance, is perfectly simple. Among the really
      expert, all organizations are instantly judged by the looks
      of their female office staff. Manager A, who cannot find an
      attractive girl for his outer office, is most unlikely to have
      found anything else. His filing system can be condemned
      without so much as a glance. What should he do about it?
      Why, he should adopt the system used by Manager B,
      whose secretary is obviously a darling. We don't know or
      care what his system may be, but it was the choice of a
      man whose discrimination has been proved.
    </p>

    <img src="ch05.0075.svg" alt="" />
    
    <p>
      Remember, however, that the attractiveness of the
      secretary must be of the right <i>kind</i>. A sultry siren is no
      better as an efficiency symbol (and is possibly worse) than
      a frumpish middle-aged spinster. Where waves of a powerful scent
      surge across the office, lapping against the filing
      cabinet and spilling into the corridor, the organization is
      liable to suffer. A deep V neckline can lead to a cleavage
      among the male staff. Such may be the effect of a swaying
      exit that hours can be wasted in getting to the bottom of
      the trouble. The office, we can safely conclude, is not the
      place for the more powerful manifestations of sex. Neither
      is it the place for the sort of sexlessness which amounts to
      hostility. The highest competence is associated with a
      bright and friendly relationship, the affection felt towards
      a favourite younger sister, the sister's pride in the achievement
      of her favourite elder brother, and a popular girl's
      trusting attitude to the world at large. All this can be
      sensed in half a minute at the reception desk. Even more
      quickly sensed are the opposite signs of self-consciousness,
      frustration or frivolity. In the presence of a frump or a
      siren, the expert knows at once that the organization is in
      a bad way and ripe for reform.
    </p>

    <p>
      What ought to be done—apart from installing a digital
      computer and apart from erecting (or demolishing) the
      partitions? The expert casts his mind back to the last
      efficient factory he saw; that, as it happens, of the Tinned
      Cabbage Company, where the blonde receptionist was
      well above average. Demure and pretty, simply dressed and
      knowing everything, eager to help and quick to smile, she
      was obviously a girl in a thousand. Now what was the
      decor at the Cabbage Factory? Her background, surely,
      had been a pastel shade of green. How would it be to
      begin the present reorganization with a change in colour
      scheme?
    </p>

    <img src="ch05.0077.svg" alt="" style="width: 50%" />

  </body>
</html>
