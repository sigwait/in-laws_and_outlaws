<?xml version="1.0" encoding="UTF-8"?>
<package version="3.0" xmlns="http://www.idpf.org/2007/opf"
         unique-identifier="bookid">
  <metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
    <dc:title>In-Laws &amp; Outlaws</dc:title>

    <dc:creator id="author1">C. Northcote Parkinson</dc:creator>
    <meta refines="#author1" property="file-as">Parkinson, C. Northcote</meta>

    <dc:language>en</dc:language>
    <!-- must be unique for each book (not revision) -->
    <dc:identifier id="bookid">urn:uuid:2c23f98d-c010-440f-a183-275e0752e0c9</dc:identifier>
    <dc:date>2022-12-07</dc:date>
    <dc:publisher>https://gitlab.com/sigwait/in-laws_and_outlaws</dc:publisher>
    <dc:rights>Copyright © 1962 C. Northcote Parkinson</dc:rights>

    <!-- update this for each revision -->
    <meta property="dcterms:modified">2022-12-08T10:00:00Z</meta>
  </metadata>

  <manifest>
    <item id="cover_xhtml" href="cover.xhtml" media-type="application/xhtml+xml" />
    <item id="cover" href="cover.jpg" media-type="image/jpeg" properties="cover-image"/>
    <item id="colophon" href="colophon.xhtml" media-type="application/xhtml+xml" />
    <item id="file0" href="colophon.jpg" media-type="image/jpeg" />
    <item id="dedication" href="dedication.xhtml" media-type="application/xhtml+xml" />
    <item id="nav" href="nav.xhtml" media-type="application/xhtml+xml" properties="nav"/>

    <item id="file1" href="style.css" media-type="text/css" />
    <item id="file2" href="hyphens.css" media-type="text/css" />

    <item id="preface" href="preface.xhtml" media-type="application/xhtml+xml" />

    <item id="file3" href="ch01.0019.svg" media-type="image/svg+xml" />
    <item id="ch01" href="ch01.xhtml" media-type="application/xhtml+xml" />

    <item id="file4" href="ch02.0033.svg" media-type="image/svg+xml" />
    <item id="ch02" href="ch02.xhtml" media-type="application/xhtml+xml" />

    <item id="file5" href="ch03.0039.svg" media-type="image/svg+xml" />
    <item id="ch03" href="ch03.xhtml" media-type="application/xhtml+xml" />

    <item id="file6" href="ch04.0059.svg" media-type="image/svg+xml" />
    <item id="ch04" href="ch04.xhtml" media-type="application/xhtml+xml" />

    <item id="file7" href="ch05.0075.svg" media-type="image/svg+xml" />
    <item id="file8" href="ch05.0077.svg" media-type="image/svg+xml" />
    <item id="ch05" href="ch05.xhtml" media-type="application/xhtml+xml" />

    <item id="ch06" href="ch06.xhtml" media-type="application/xhtml+xml" />

    <item id="file9" href="ch07.0099.svg" media-type="image/svg+xml" />
    <item id="ch07" href="ch07.xhtml" media-type="application/xhtml+xml" />

    <item id="ch08" href="ch08.xhtml" media-type="application/xhtml+xml" />

    <item id="file10" href="ch09.0129.svg" media-type="image/svg+xml" />
    <item id="ch09" href="ch09.xhtml" media-type="application/xhtml+xml" />

    <item id="file11" href="ch10.0143.svg" media-type="image/svg+xml" />
    <item id="file12" href="ch10.0162.png" media-type="image/png" />
    <item id="file13" href="ch10.0164.svg" media-type="image/svg+xml" />
    <item id="ch10" href="ch10.xhtml" media-type="application/xhtml+xml" />

    <item id="ch11" href="ch11.xhtml" media-type="application/xhtml+xml" />

    <item id="file14" href="ch12.0182.svg" media-type="image/svg+xml" />
    <item id="file15" href="ch12.0183.svg" media-type="image/svg+xml" />
    <item id="file16" href="ch12.0185.svg" media-type="image/svg+xml" />
    <item id="ch12" href="ch12.xhtml" media-type="application/xhtml+xml" />
  </manifest>

  <spine>
    <itemref idref="cover_xhtml" linear="no" />
    <itemref idref="colophon" />
    <itemref idref="dedication" />
    <itemref idref="nav" />
    <itemref idref="preface" />
    <itemref idref="ch01" />
    <itemref idref="ch02" />
    <itemref idref="ch03" />
    <itemref idref="ch04" />
    <itemref idref="ch05" />
    <itemref idref="ch06" />
    <itemref idref="ch07" />
    <itemref idref="ch08" />
    <itemref idref="ch09" />
    <itemref idref="ch10" />
    <itemref idref="ch11" />
    <itemref idref="ch12" />
  </spine>
</package>
